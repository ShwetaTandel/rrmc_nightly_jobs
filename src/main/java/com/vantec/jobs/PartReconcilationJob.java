package com.vantec.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PartReconcilationJob {

	public static void callPartReconcilationJob() {
		try {
			// copy file ZIF_LSP_STOCK.OUT from shared cddata to local path

			JobsHelper.copyFile("\\\\172.18.21.5\\cddata\\prod\\ZIF_LSP_STOCK.OUT",
					"C:\\RRMCJobs\\PartRecon\\ZIF_LSP_STOCK.OUT");
			System.out.println("Copy file successful");

			System.out.println("Get DB Connection...");
			Connection conn = JobsHelper.getConnection();
			System.out.println("Create tables...");
			createReconcilationTable(conn);
			System.out.println("Insert initial data...");
			insertReconWithPartData(conn);
			System.out.println("Update 1 ....");
			updateReconWithRRMCStock(conn, "C:\\RRMCJobs\\PartRecon\\ZIF_LSP_STOCK.OUT");
			System.out.println("Update 2 ....");
			writeInventoryDataToVIMSstockFile(conn, "C:\\RRMCJobs\\PartRecon\\VIMS_LSP_STOCK.OUT");
			System.out.println("Update 3 ....");
			updateReconcWithVIMSQty(conn, "C:\\RRMCJobs\\PartRecon\\VIMS_LSP_STOCK.OUT");
			System.out.println("Write CSV file ....");
			writeReconcilationToCSV(conn, "C:\\RRMCJobs\\PartRecon\\RECONC.CSV");
			new File("\\\\172.18.21.5\\cddata\\prod\\ZIF_LSP_STOCK.OUT").delete();
			System.out.println("Email CSV File ....");
			List<String> fileList = new ArrayList<String>();
			fileList.add("C:\\RRMCJobs\\PartRecon\\RECONC.CSV");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			JobsHelper.sendMail("shweta-tandel.ce@vantec-gl.com", fileList,
					"RRMC Reconciliation " + sdf.format(dt));
			JobsHelper.sendMail(JobsHelper.getEmailsForJob("RRMC_RECONC", conn), fileList,
					"RRMC Reconciliation " + sdf.format(dt));

			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}

	/**
	 * Return a JDBC Connection
	 */

	// SQL to Create Table
	private static void createReconcilationTable(Connection conn) {
		try {
			Statement st = conn.createStatement();
			st.executeUpdate(
					"CREATE TABLE IF NOT EXISTS vantec.E2RECONC (id bigint AUTO_INCREMENT PRIMARY KEY, version bigint, date_created timestamp,part_id bigint, part_number varchar(20), part_description varchar(35), rrmc_qty dec(9,3), vims_qty dec(9,3), diff_qty dec(9,3), miss_qty dec(9,3), qa_qty dec(9,3), rrmc_opqty dec(9,3))");
			st.executeUpdate("DELETE FROM vantec.E2RECONC WHERE id >= 0");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// SQL to Insert Values
	private static void insertReconWithPartData(Connection conn) {
		String sqlStr = "";
		String sqlInsText = "";
		String desc = "";
		int id = 0;
		String partNumber = "";

		System.out.println("Inserting Records...");
		// Read Parts Data
		sqlStr = "SELECT a.id, max(a.part_number) as part_number, max(a.part_description) as part_description, max(a.conversion_factor) as conversion_factor FROM vantec.part a WHERE a.vendor_id=866 AND NOT EXISTS( SELECT b.part_number FROM vantec.E2RECONC b WHERE b.part_number = a.part_number) GROUP BY a.id";
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			// java.sql.Date date = new java.sql.Date();
			while (rs.next()) {
				desc = rs.getString("part_description");
				desc = desc.replaceAll("'", "");
				desc = desc.replaceAll(",", "");
				id = rs.getInt("id");
				partNumber = rs.getString("part_number");
				Statement stInsert = conn.createStatement();
				sqlInsText = "INSERT INTO vantec.E2RECONC VALUES(DEFAULT,0,now()," + id + ",'" + partNumber + "','"
						+ desc + "',0,0,0,0,0,0)";
				stInsert.executeUpdate(sqlInsText);
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	// Populate table with RRMC Stock Qty from the ZIF FILE
	private static void updateReconWithRRMCStock(Connection conn, String fileName) {
		String lineTxt = "";
		String rrmcPart = "";
		String rrmcQty = "";
		String rrmcOpQty = "";
		try {
			// Reset RRMC Qty
			Statement st = conn.createStatement();
			st.executeUpdate("UPDATE vantec.E2RECONC SET rrmc_qty = 0, rrmc_opqty = 0");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		File file = new File(fileName);
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				//System.out.println(line);
				lineTxt = line.trim();
				// Strip out Break Characters
				lineTxt = lineTxt.replaceAll("\t", "");
				lineTxt = lineTxt.replaceAll("\n", "");
				lineTxt = lineTxt.replaceAll("\r", "");
				if (lineTxt.length() > 10) {
					// Parse Fields
					try {
						rrmcPart = lineTxt.split("!")[2];
						rrmcQty = lineTxt.split("!")[3];
					} catch (Exception ex) {
						rrmcPart = "";
						rrmcQty = "";
					}
					try {
						rrmcOpQty = lineTxt.split("!")[5];
					} catch (Exception ex) {
						rrmcOpQty = "";
					}
					//System.out.println("RRMC: " + rrmcPart + " Qty: " + rrmcQty);
					if (rrmcPart != "") {
						try {
							Statement st = conn.createStatement();
							st.executeUpdate("UPDATE vantec.E2RECONC SET rrmc_qty = rrmc_qty + " + rrmcQty
									+ ", rrmc_opqty = rrmc_opqty + " + rrmcOpQty + " WHERE part_number = '"
									+ rrmcPart.trim() + "'");
							//System.out.println("Updating" + rrmcPart.trim() + "<n");
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
			br.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	// Populate Inventory from Vims Database in file
	private static void writeInventoryDataToVIMSstockFile(Connection conn, String fileName) {
		String sqlStr = "";
		String qty = "0.000";
		String lineStr = "";

		// Delete existing file
		new File(fileName).delete();
		// Create a new one
		try {
			FileWriter fw = new FileWriter(fileName);

			// Select Inventory
			sqlStr = "SELECT a.part_id, max(b.conversion_factor) as conversion_factor, sum(a.inventory_qty) as inventory_qty, max(c.location_code) as location_code, max(c.location_type_id) as location_type_id FROM vantec.inventory_master a LEFT JOIN vantec.part b ON a.part_id=b.id LEFT JOIN vantec.location c ON a.current_location_id = c.id WHERE ((substr(c.location_code,1,4)='MISS') or (substr(c.location_code,1,4)<>'PICK' AND location_type_id <> 4 and location_type_id <> 6)) group by a.part_id order by a.part_id";
			try {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(sqlStr);
				while (rs.next()) {
					try {
						qty = String.format("%.3f", rs.getInt("inventory_qty") / rs.getInt("conversion_factor"));
					} catch (Exception ex) {
						qty = "0.000";
					}
					//System.out.println("VIMS: " + rs.getInt("part_id") + " Qty: " + qty + " Loc: "+ rs.getInt("location_type_id"));
					// Output Stock Qty
					lineStr = rs.getInt("part_id") + "!" + qty + "!" + "0" + "!" + "0" + "\r\n";
					fw.write(lineStr);
				}
				// Select MISS01
				sqlStr = "SELECT a.part_id, max(b.conversion_factor) as conversion_factor, sum(a.inventory_qty) as inventory_qty, max(c.location_code) as location_code, max(c.location_type_id) as location_type_id FROM vantec.inventory_master a LEFT JOIN vantec.part b ON a.part_id=b.id LEFT JOIN vantec.location c ON a.current_location_id = c.id WHERE (substr(c.location_code,1,4)='MISS') group by a.part_id order by a.part_id";
				rs = st.executeQuery(sqlStr);
				while (rs.next()) {
					try {
						qty = String.format("%.3f", rs.getInt("inventory_qty") / rs.getInt("conversion_factor"));
					} catch (Exception ex) {
						qty = "0.000";
					}
					//System.out.println("VIMS: " + rs.getInt("part_id") + " Qty: " + qty + " Loc: "+ rs.getInt("location_type_id"));
					// Output Stock Qty
					lineStr = rs.getInt("part_id") + "!" + "0" + "!" + qty + "!" + "0" + "\r\n";
					fw.write(lineStr);
				}
				// Select QA
				sqlStr = "SELECT a.part_id, max(b.conversion_factor) as conversion_factor, sum(a.inventory_qty) as inventory_qty, max(c.location_code) as location_code, max(c.location_type_id) as location_type_id FROM vantec.inventory_master a LEFT JOIN vantec.part b ON a.part_id=b.id LEFT JOIN vantec.location c ON a.current_location_id = c.id WHERE (c.location_type_id=4) group by a.part_id order by a.part_id";
				rs = st.executeQuery(sqlStr);
				while (rs.next()) {
					try {
						qty = String.format("%.3f", rs.getInt("inventory_qty") / rs.getInt("conversion_factor"));
					} catch (Exception ex) {
						qty = "0.000";
					}
				//	System.out.println("VIMS: " + rs.getInt("part_id") + " Qty: " + qty + " Loc: "+ rs.getInt("location_type_id"));
					// Output Stock Qty
					lineStr = rs.getInt("part_id") + "!" + "0" + "!" + "0" + "!" + qty + "\r\n";
					fw.write(lineStr);
				}
				fw.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Populate VIMS Stock Qty
	public static void updateReconcWithVIMSQty(Connection conn, String fileName) {
		String lineTxt = "";
		String part_id = "";
		String qty = "";
		String miss_qty = "";
		String qa_qty = "";
		String sqlSelText = "";
		try {
			// Reset RRMC Qty
			Statement st = conn.createStatement();
			st.executeUpdate("UPDATE vantec.E2RECONC SET vims_qty = 0, miss_qty = 0, qa_qty = 0");

			// Read RRMC Stock Data
			File file = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				lineTxt = line.trim();
				// Strip out Break Characters
				lineTxt = lineTxt.replaceAll("\t", "");
				lineTxt = lineTxt.replaceAll("\n", "");
				lineTxt = lineTxt.replaceAll("\r", "");
				if (lineTxt.length() > 10) {
					// Parse Fields
					try {
						part_id = lineTxt.split("!")[0];
						qty = lineTxt.split("!")[1];
						miss_qty = lineTxt.split("!")[2];
						qa_qty = lineTxt.split("!")[3];
					} catch (Exception ex) {
						part_id = "";
						qty = "0";
						miss_qty = "0";
						qa_qty = "0";
					}
					//System.out.println("VIMS: " + part_id + " Qty: " + qty + " Miss: " + miss_qty + " QA: " + qa_qty);
					if ((part_id != "") && (part_id != null)) {
						// Update Qty
						sqlSelText = "UPDATE vantec.E2RECONC SET vims_qty=vims_qty+" + qty + ", miss_qty=miss_qty+"
								+ miss_qty + ", qa_qty=qa_qty+" + qa_qty + " WHERE part_id = '" + part_id + "'";
						st.executeUpdate(sqlSelText);

					}
				}
			}
			br.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	// Reconc to CSV format
	public static void writeReconcilationToCSV(Connection conn, String fileName) {
		String sqlStr = "";
		String lineStr = "";
		new File(fileName).delete();
		try {

			FileWriter fw = new FileWriter(fileName);
			lineStr = "Part_Number,Description,RRMC_qty,VIMS_qty,Diff_qty,Miss_qty,QA_qty,OPPick_Qty,Date_Created"
					+ "\r\n";
			fw.append(lineStr);
			Statement st = conn.createStatement();
			// Update Difference Qty
			System.out.println("Calculating Stock Difference...");
			sqlStr = "UPDATE vantec.E2RECONC SET diff_qty = rrmc_qty - vims_qty";
			st.executeUpdate(sqlStr);

			// Delete zero Qty Records
			System.out.println("Deleting  Records with zero qty...");
			sqlStr = "DELETE from vantec.E2RECONC WHERE rrmc_qty=0 AND vims_qty =0";
			st.executeUpdate(sqlStr);

			sqlStr = "SELECT * FROM vantec.E2RECONC";
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				lineStr = rs.getString("part_number").trim() + "," + rs.getString("part_description").trim() + ","
						+ rs.getInt("rrmc_qty") + "," + rs.getInt("vims_qty") + "," + rs.getInt("diff_qty") + ","
						+ rs.getInt("miss_qty") + "," + rs.getInt("qa_qty") + "," + rs.getInt("rrmc_opqty") + ","
						+ new Date() + "\r\n";
				fw.append(lineStr);
			}
			fw.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
