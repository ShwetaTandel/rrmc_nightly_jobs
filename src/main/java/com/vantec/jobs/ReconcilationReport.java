package com.vantec.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//IDOC Input Mapping of Flat File Data

public class ReconcilationReport {

	private static int status = 0;

	public static void callReconcilationReport() {

		String sqlStr = "";
		try {
			JobsHelper.copyFile("\\\\172.18.21.5\\cddata\\prod\\ZIF_LSP_STOCK.OUT",
					"C:\\RRMCJobs\\ReconJob\\ZIF_LSP_STOCK.OUT");
			System.out.println("Copy ZIF_LSP_STOCK.OUT file successful from cd data to Recon Job");
		} catch (IOException e) {
			System.out.println("File Not Found");
			e.printStackTrace();
		}

		try {
			// Create DB Connection*/
			System.out.println("Creating DB Connection....");
			Connection conn = JobsHelper.getConnection();
			// Create Reconc Table & Populate Static Data
			System.out.println("Create RECONC table....");
			sqlCreateTable(conn);
			System.out.println("Inserting Records in RECONC.....");
			sqlInsertData(conn);
			// Populate RRMC Stock Data
			System.out.println("Populate RRMC Stock DATA from the file.....");
			rrmcUpdateData(conn, "C:\\RRMCJobs\\ReconJob\\ZIF_LSP_STOCK.OUT");

			// Populate VIMS Stock Data
			System.out.println("Update Quantities for the RRMC Stock DATA ....");
			vimsUpdateData(conn);

			// Update Difference Qty
			System.out.println("Calculating Stock Difference...");
			sqlStr = "UPDATE vantec.E2RECONC SET diff_qty = rrmc_qty - vims_qty";
			Statement st = conn.createStatement();
			st.executeUpdate(sqlStr);

			// Delete zero Qty Records
			System.out.println("Deleting Zero Records...");
			sqlStr = "DELETE from vantec.E2RECONC WHERE rrmc_qty=0 AND vims_qty =0";
			st.execute(sqlStr);
			
			// Reconc CSV File
			System.out.println("Creating Output CSV Data...");
			reconcCsv(conn, "C:\\RRMCJobs\\ReconJob\\RECONC.CSV");

			List<String> fileList = new ArrayList<String>();
			fileList.add("C:\\RRMCJobs\\ReconJob\\RECONC.CSV");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			JobsHelper.sendMail("shweta-tandel.ce@vantec-gl.com", fileList, "RRMC Reconciliation " + sdf.format(dt));
			System.out.println("Sending emails....");

			JobsHelper.sendMail(JobsHelper.getEmailsForJob("RECONC_REPORT", conn), fileList,
					"RRMC Reconciliation " + sdf.format(dt));

			// Close SQL
			conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}

		System.exit(status);
		return;
	}

	// SQL to Create Table
	public static void sqlCreateTable(Connection conn) {
		try {
			String sqlText = "DROP TABLE IF EXISTS vantec.E2RECONC";
			Statement st = conn.createStatement();
			st.executeUpdate(sqlText);

			sqlText = "CREATE TABLE IF NOT EXISTS vantec.E2RECONC (id bigint AUTO_INCREMENT PRIMARY KEY, version bigint, date_created timestamp,part_id bigint, part_number varchar(20), part_description varchar(35), rrmc_qty dec(9,3), vims_qty dec(9,3), diff_qty dec(9,3), miss_qty dec(9,3), qa_qty dec(9,3),rrmc_opqty dec(9,3))";

			st.executeUpdate(sqlText);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// SQL to Insert Values
	private static void sqlInsertData(Connection conn) {
		String sqlStr = "";
		String sqlInsText = "";
		String DESC = "";
		try {
			// Read Parts Data
			sqlStr = "SELECT a.id, max(a.part_number) as part_number, max(a.part_description) as part_description, max(a.conversion_factor) as conversion_factor FROM vantec.part a WHERE NOT EXISTS( SELECT * FROM vantec.E2RECONC b WHERE b.part_number = a.part_number) GROUP BY a.id";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				DESC = rs.getString("part_description");
				DESC = DESC.replaceAll("'", "");
				DESC = DESC.replaceAll(",", "");

				// Insert Records from Parts Master / Inventory Detail
				sqlInsText = "INSERT INTO vantec.E2RECONC VALUES(DEFAULT,0,now()," + rs.getInt("id") + ",'"
						+ rs.getString("part_number") + "','" + DESC + "',0,0,0,0,0,0)";
				Statement insertSt = conn.createStatement();
				insertSt.executeUpdate(sqlInsText);
			}
		} catch (SQLException se) {
			System.out.println(sqlInsText);
			System.out.println("Error in inserting " + se);
		}
	}

	// Populate RRMC Stock Qty
	private static void rrmcUpdateData(Connection conn, String fileName) {
		String lineTxt = "";
		String rrmcPart = "";
		String rrmcQty = "";
		String sqlSelText = "";

		// Read RRMC Stock Data
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = br.readLine()) != null) {
				lineTxt = line.trim();
				// Strip out Break Characters
				lineTxt = lineTxt.replaceAll("\t", "");
				lineTxt = lineTxt.replaceAll("\n", "");
				lineTxt = lineTxt.replaceAll("\r", "");

				if (lineTxt.length() > 10) {
					// Parse Fields
					try {
						rrmcPart = lineTxt.split("!")[2];
						rrmcQty = lineTxt.split("!")[3];
					} catch (Exception e) {
						rrmcPart = "";
						rrmcQty = "";
					}
					if (!rrmcPart.equals("")) {
						// Update RRMC Qty
						sqlSelText = "UPDATE vantec.E2RECONC SET rrmc_qty = rrmc_qty + " + rrmcQty
								+ " WHERE part_number = '" + rrmcPart.trim() + "'";
						Statement updateSt = conn.createStatement();
						updateSt.executeUpdate(sqlSelText);
					}
				}
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Populate Vims Data
	private static void vimsUpdateData(Connection conn) {
		String sqlStr = "";
		String sqlSelText = "";
		String QTY = "0.000";
		try {
			// Select Inventory
			sqlStr = "SELECT a.part_id, b.conversion_factor, a.inventory_qty, b.part_number, c.inventory_status_code FROM vantec.inventory_master a LEFT JOIN vantec.part b ON a.part_id=b.id LEFT JOIN vantec.inventory_status c ON a.inventory_status_id = c.id";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				try {
					QTY = String.format("%.3f", (double) (rs.getInt("inventory_qty") / rs.getInt("conversion_factor")));
				} catch (Exception ex) {
					QTY = "0.000";
					System.out.println("Setting Quantity zero for part="+rs.getInt("part_id"));
					System.out.println("Exception is" + ex);

				}
				if (rs.getString("inventory_status_code") != null) {
					// Update Stock Qty
					if (rs.getString("inventory_status_code").equals("OK")
							|| rs.getString("inventory_status_code").equals("PLT")) {
						sqlSelText = "UPDATE vantec.E2RECONC SET vims_qty = vims_qty + " + QTY + " WHERE part_id = "
								+ rs.getInt("part_id");
						Statement updateSt = conn.createStatement();
						updateSt.executeUpdate(sqlSelText);
						//System.out.println(sqlSelText);
					}
					if (rs.getString("inventory_status_code").equals("MISSING")
							|| rs.getString("inventory_status_code").equals("PI")) {

						sqlSelText = "UPDATE vantec.E2RECONC SET miss_qty = miss_qty + " + QTY + " WHERE part_id = "
								+ rs.getInt("part_id");
						Statement updateSt = conn.createStatement();
						updateSt.executeUpdate(sqlSelText);
					}
					if (rs.getString("inventory_status_code").equals("QA")) {
						sqlSelText = "UPDATE vantec.E2RECONC SET qa_qty = qa_qty + " + QTY + " WHERE part_id = "
								+ rs.getInt("part_id");
						Statement updateSt = conn.createStatement();
						updateSt.executeUpdate(sqlSelText);
					}
				}else{
					System.out.println("Inventory Status null for part="+rs.getInt("part_id"));
				}
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	// Reconc to CSV format
	private static void reconcCsv(Connection conn, String fileName) {
		String sqlStr = "";
		String lineStr = "";
		try {
			new File(fileName).delete();
			FileWriter wf = new FileWriter(fileName);
			lineStr = "Part_Number,Description,RRMC_qty,VIMS_qty,Diff_qty,Miss_qty,QA_qty,Date_Created" + "\r\n";
			wf.write(lineStr);
			sqlStr = "SELECT * FROM vantec.E2RECONC";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				lineStr = rs.getString("part_number").trim() + "," + rs.getString("part_description").trim() + ","
						+ rs.getInt("rrmc_qty") + "," + rs.getInt("vims_qty") + "," + rs.getInt("diff_qty") + ","
						+ rs.getInt("miss_qty") + "," + rs.getInt("qa_qty") + ","
						+ rs.getString("date_created").substring(0, 10) + "\r\n";
				wf.write(lineStr);
			}
			wf.close();
		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (SQLException se) {
			se.printStackTrace();
		}

	}

}
