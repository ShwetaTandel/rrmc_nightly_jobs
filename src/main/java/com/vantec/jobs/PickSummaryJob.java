package com.vantec.jobs;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PickSummaryJob {

	private static int status = 0;

	public static void callPickSummaryJob() {

		try {
			// Store TimeStamp
			Connection conn = JobsHelper.getConnection();
			// Create Reconc Table & Populate Static Data
			System.out.println("Create Table... ");
			sqlCreateTable(conn);
			System.out.println("Inserting records... ");
			sqlInsertData(conn);

			// Close SQL
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(status);
		return;
	}

		// SQL to Create Table
	public static void sqlCreateTable(Connection conn) {

		String sqlText = "";
		try {
			Statement st = conn.createStatement();
			sqlText = "CREATE TABLE IF NOT EXISTS vantec.picksummary (id bigint AUTO_INCREMENT PRIMARY KEY, expected_delivery timestamp,pick_type varchar(20), tanum varchar(20), order_lines bigint, serials bigint, allocated bigint, picked bigint, outstanding bigint, marshalled bigint, hard_shortage bigint, soft_shortage bigint, F6_shortage bigint, transmitted bigint)";
			st.executeUpdate(sqlText);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// SQL to Insert Values
	public static void sqlInsertData(Connection conn) {
		String sqlDelText = "";
		String sqlInsText = "";

		String $mDataQuery = "";
		Date $mDelivery;
		String $mPickType = "";
		String $mTanum = "";
		String $mPartNumber = "";
		int $mPickId = 0;
		int $mTransFlag = 0;
		int $mOrderLines = 0;
		int $mSerialLines = 0;
		int $mAllocLines = 0;
		int $mPickLines = 0;
		int $mMarshalLines = 0;
		int $mHardShortLines = 0;
		int $mSoftShortLines = 0;
		int $mF6ShortLines = 0;
		System.out.println("Inserting Records...");
		// Read Data
		$mDataQuery = "SELECT  id AS hdr_id, expected_delivery AS delivery, bwlvs AS pick_type,customer_reference_code AS customer_reference_code, picked AS picked, transmitted AS transmitted FROM vantec.document_header WHERE document_type = 'RRM Order' AND (customer_reference_code IS NOT NULL)";
		$mDataQuery = $mDataQuery + " AND (date(last_updated) >= curdate() or date(expected_delivery) >= SUBDATE(CURDATE(),1))";
		$mPickType = "";
		$mTanum = "";
		$mTransFlag = 0;
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery($mDataQuery);
			while (rs.next()) {
				$mDelivery = rs.getDate("delivery");
				$mTanum = rs.getString("customer_reference_code");
				String pickType = rs.getString("pick_type");
				if (pickType.equals("943")) {
					$mPickType = "943 - CSET";
				} else if (pickType.equals("945")) {
					$mPickType = "945 - CSET";
				} else if (pickType.equals("913")) {
					$mPickType = "913 - EMERG";
				} else if (pickType.equals("350")) {
					$mPickType = "350 - GLTP";
				} else {
					$mPickType = pickType + " - ";
				}

				if ((rs.getBoolean("transmitted") == true)) {
					$mTransFlag = 1;
				} else {
					$mTransFlag = 0;
				}

				$mOrderLines = 0;
				$mAllocLines = 0;
				$mPickLines = 0;
				$mSerialLines = 0;
				$mMarshalLines = 0;
				$mHardShortLines = 0;
				$mSoftShortLines = 0;
				$mF6ShortLines = 0;
				String $mPickQuery = "SELECT id as pick_hdr_id FROM vantec.document_header WHERE document_type = 'Pick' AND customer_reference_code = '"
						+ $mTanum + "' LIMIT 1";
				Statement pickSt = conn.createStatement();
				ResultSet pickRs = pickSt.executeQuery($mPickQuery);
				while (pickRs.next()) {
					$mPickId = pickRs.getInt("pick_hdr_id");
				}

				String $mBodyQuery = "SELECT b.id AS body_id, b.qty_expected AS body_qty_expected, b.qty_transacted AS body_qty_transacted, b.part_id AS body_part_id FROM vantec.document_body b WHERE b.document_header_id ="
						+ $mPickId;

				Statement bodySt = conn.createStatement();
				ResultSet bodyRs = bodySt.executeQuery($mBodyQuery);
				while (bodyRs.next()) {
					int $mPartId = bodyRs.getInt("body_part_id");
					int $mBodyQtyExpected = bodyRs.getInt("body_qty_expected");
					int $mBodyQtyTransacted = bodyRs.getInt("body_qty_transacted");
					int $mBodySerialLines = 0;
					int $mBodyAllocLines = 0;
					int $mBodyPickLines = 0;

					String $mDtlQuery = "SELECT c.id as dtl_id, c.qty_expected as dtl_qty_expected, c.qty_transacted as dtl_qty_transacted FROM vantec.document_detail c WHERE c.document_body_id = "
							+ bodyRs.getInt("body_id");

					Statement detlSt = conn.createStatement();
					ResultSet detlRs = detlSt.executeQuery($mDtlQuery);
					while (detlRs.next()) {
						int $mDtlQtyTransacted = detlRs.getInt("dtl_qty_transacted");

						$mBodySerialLines++;
						if (detlRs.getInt("dtl_qty_expected") > 0) {
							$mOrderLines++;
						}
						String $mAllocQuery = "SELECT allocated_qty AS allocated_qty, processed AS processed, inventory_master_id AS inventory_master_id FROM vantec.allocation_detail WHERE originating_request_id="
								+ detlRs.getInt("dtl_id") + " LIMIT 1";

						int $mAllocCount = 0;
						Statement allocSt = conn.createStatement();
						ResultSet allocRs = allocSt.executeQuery($mAllocQuery);
						while (allocRs.next()) {
							$mAllocCount++;
							if ((allocRs.getInt("allocated_qty") > 0) || ((allocRs.getBoolean("processed") == true)
									|| (allocRs.getInt("processed") > 0))) {
								$mBodyAllocLines++;
							}
							// Test for stock in location 38880 (MISS03) - F6
							// Shortage.
							if ($mDtlQtyTransacted > 0) {
								String $mInvQuery = "SELECT id AS inv_id, current_location_id AS current_location_id FROM vantec.inventory_master WHERE id="
										+ allocRs.getInt("inventory_master_id") + " LIMIT 1";
								Statement invSt = conn.createStatement();
								ResultSet invRs = invSt.executeQuery($mInvQuery);
								while (invRs.next()) {
									if (invRs.getInt("current_location_id") == 38880) {
										$mF6ShortLines++;
									}
								}
							}
						}
					}
					// Picked Serial Count
					if ($mBodyQtyTransacted > 0) {
						String $mPartQuery = "SELECT part_number as part_number FROM vantec.part WHERE id=" + $mPartId
								+ " LIMIT 1";
						Statement partSt = conn.createStatement();
						ResultSet partRs = partSt.executeQuery($mPartQuery);
						while (partRs.next()) {
							$mPartNumber = partRs.getString("part_number");
						}
						String $mTranQuery = "SELECT id as tran_id FROM vantec.transaction_history WHERE ran_or_order='"
								+ $mTanum + "' AND part_number='" + $mPartNumber + "' LIMIT 10";
						Statement tranSt = conn.createStatement();
						ResultSet tranRs = tranSt.executeQuery($mTranQuery);
						while (tranRs.next()) {
							$mBodyPickLines++;
						}
					}
					// Fudge to cater for Picked > Allocated
					if ($mBodyPickLines > $mBodyAllocLines) {
						$mBodyAllocLines = $mBodyPickLines;
					}
					if ($mBodyPickLines > $mBodySerialLines) {
						$mBodySerialLines = $mBodyPickLines;
					}
					// Check for Stock Shortages
					int $mFreeStock = 0;
					if ($mBodySerialLines > $mBodyAllocLines) {
						String $mInvQuery = "SELECT d.id AS inv_id, d.allocated_qty AS allocated_qty, d.inventory_qty AS inventory_qty FROM vantec.inventory_master d WHERE d.part_id = "
								+ $mPartId;

						Statement invenSt = conn.createStatement();
						ResultSet invenRs = invenSt.executeQuery($mInvQuery);
						while (invenRs.next()) {
							$mFreeStock = $mFreeStock
									+ (invenRs.getInt("inventory_qty") - invenRs.getInt("allocated_qty"));
						}
						if ($mFreeStock == 0) {
							$mHardShortLines++;
						} else {
							$mSoftShortLines++;
						}
					}
					$mSerialLines = $mSerialLines + $mBodySerialLines;
					$mAllocLines = $mAllocLines + $mBodyAllocLines;
					$mPickLines = $mPickLines + $mBodyPickLines;
				}
				// Fudge to cater for Picked > Allocated
				if (($mPickLines >= $mSerialLines) && ($mPickLines > $mAllocLines)) {
					$mAllocLines = $mPickLines;
				}

				// Delete Records
				sqlDelText = "DELETE FROM vantec.picksummary WHERE expected_delivery = '" + $mDelivery
						+ "' AND tanum = '" + $mTanum + "'";
				Statement deleteSt = conn.createStatement();
				deleteSt.executeUpdate(sqlDelText);
				// Insert Records
				sqlInsText = "INSERT INTO vantec.picksummary VALUES(DEFAULT,'" + $mDelivery + "','" + $mPickType + "','"
						+ $mTanum + "'," + $mOrderLines + "," + $mSerialLines + "," + $mAllocLines + "," + $mPickLines
						+ "," + ($mSerialLines - $mPickLines) + "," + $mMarshalLines + "," + $mHardShortLines + ","
						+ $mSoftShortLines + "," + $mF6ShortLines + "," + $mTransFlag + ")";
				Statement insertSt = conn.createStatement();


				insertSt.executeUpdate(sqlInsText);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
