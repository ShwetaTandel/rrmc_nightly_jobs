package com.vantec.jobs;

import java.io.IOException;
import java.sql.SQLException;


public class MainJob {
	
	
	public static void main(String args[]) throws SQLException, IOException{
		
		String jobName = args[0];
		if(jobName!=null){
			if(jobName.equalsIgnoreCase("DailyReports")){
				DailyReports.callDailyReports();
				
			}else if(jobName.equalsIgnoreCase("PartReconcilationJob")){
				PartReconcilationJob.callPartReconcilationJob();
				
			}else if(jobName.equalsIgnoreCase("PartUpdateJob")){
				PartUpdateJob.callPartUpdateJob();
				
			}else if(jobName.equalsIgnoreCase("PickSummaryJob")){
				PickSummaryJob.callPickSummaryJob();
				
			}else if(jobName.equalsIgnoreCase("ReconcilationReport")){
				ReconcilationReport.callReconcilationReport();
				
			}else if(jobName.equalsIgnoreCase("TrailerReport")){
				TrailerReport.callTrailerReport();
				
			}
		}
		
	}
	
	
}
