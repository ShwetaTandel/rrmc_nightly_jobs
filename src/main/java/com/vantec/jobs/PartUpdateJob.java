package com.vantec.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PartUpdateJob {

	public static void callPartUpdateJob() {

		try {
			
			// copy file ZIF_LSP_MM.OUT from shared cddata to local path
			copyFile("\\\\172.18.21.5\\cddata\\prod\\ZIF_LSP_MM.OUT", "C:\\RRMCJobs\\PartUpdate\\ZIF_LSP_MM.OUT");
			System.out.println("Copy file successful");
			// Main Process
			Connection conn = JobsHelper.getConnection();
			// Drop Table if it exists
			dropTable(conn);
			// Wite idocinpartout.xml file
			writeXMLFile(conn);
			// PART update
			partInsert(conn);

			// PART update qa
			partUpdateQA(conn);

			// PART updated vendor part update
			partUpdateVendorPart(conn);

			// PART update location type
			partUpdateLocationType(conn);
			new File("\\\\172.18.21.5\\cddata\\prod\\ZIF_LSP_MM.OUT").delete();
			List<String> fileList = new ArrayList<String>();
			fileList.add("C:\\RRMCJobs\\PartUpdate\\ZIF_LSP_MM.OUT");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			JobsHelper.sendMail("shweta-tandel.ce@vantec-gl.com", fileList, "RRMC_Parts_File "+sdf.format(dt));
			JobsHelper.sendMail(JobsHelper.getEmailsForJob("PART_UPDATE", conn), fileList, "RRMC_Parts_File "+sdf.format(dt));
			conn.close();
		} catch (Exception ex) {
			
			System.out.println(ex);

		}

	}
	
	private static void checkTestMethod(){
		Part part = new Part();
		part.setId("DEFAULT");
		part.setVersion("0");
		part.setDateCreated("now()");
		part.setEffectiveFrom("now()");
		part.setEffectiveTo("2035-12-31 23:59:59");
		part.setFixedLocationId("null");
		part.setFixedLocationTypeId("null");
		part.setFixedLocationSubTypeId("null");
		part.setImaginaryPart("0");
		part.setLastUpdated("now()");
		part.setLastUpdatedBy("ADDN");
		part.setPartDescription("DESC");
		part.setPartNumber("MATNR");
		part.setRequiresCount("0");
		part.setRequiresDecant("0");
		part.setRequiresInspection("0");
		part.setSafetyStock("0");
		part.setShelfLife("0");
		part.setVendorId("866");
		part.setVendorPartCode("VENDOR_PART_CODE");
		part.setWiCode("A1");
		part.setConversionFactor("1000");
		part.setFullBoxPick("0");
		part.setActive("1");
		part.setRdtPrompt("");
		part.setJisType("JIS_TYPE");
		part.setModular("0");
		part.setPartFamily("PART_FAMILY");
		part.setUom("UOM");
		part.setIncludeInventory("0");
		part.setLoosePartQty("0");
		part.setMaxPickFaceQty("0");
		part.setWeight("0");
		part.setUpdateSerialWithCount("0");
		part.setPackTypeId("null");
		part.setMiniStock("0");
		part.setImportanta("0");
		part.setHighValue("0");
		part.setDecantChargeId("null");
		part.setDespatchChargeId("null");
		part.setInspectChargeId("null");
		part.setReceiptChargeId("null");
		part.setStorageChargeId("null");
		part.setTransportChargeId("null");
		part.setCreatedBy("ADDN");
		part.setAutoPick("0");
		part.setAutoReceive("0");
		part.setJisSupplyGroup("");
		part.setPiecePartNumber("null");
		part.setVendorName("");
		part.setReplenPriority("null");

		
		
		
		String sqlText = "INSERT INTO vantec.part VALUES(" 
		+ part.getId() 
		+ "," + part.getVersion()
		+ "," +  part.getDateCreated()
		+ "," +  part.getEffectiveFrom()
		+ ",'" + part.getEffectiveTo() + "'"
		+ "," +  part.getFixedLocationId()
		+ "," +  part.getFixedLocationTypeId()
		+ "," + part.getFixedLocationSubTypeId()
		+ "," + part.getImaginaryPart()
		+ "," + part.getLastUpdated()
		+ ",'" + part.getLastUpdatedBy() + "'"
		+ ",'" + part.getPartDescription() + "'"
		+ ",'"  + part.getPartNumber() + "'"
		+ "," + part.getRequiresCount() 
		+ "," + part.getRequiresDecant()
		+ "," + part.getRequiresInspection()
		+ "," + part.getSafetyStock()
		+ "," + part.getShelfLife()
		+ "," + part.getVendorId()
		+ ",'"+ part.getVendorPartCode() + "'"
		+ ",'"+ part.getWiCode() + "'" 
		+ "," + part.getConversionFactor()
		+ "," + part.getFullBoxPick()
		+ "," + part.getActive()
		+ ",'"+ part.getRdtPrompt()+"'"
		+",'"+ part.getJisType()+"'"
		+ "," + part.getModular()
		+",'"+ part.getPartFamily()+"'"
		+",'" + part.getUom()+"'"
		+ "," + part.getIncludeInventory()
		+ "," + part.getLoosePartQty()
		+ "," + part.getMaxPickFaceQty()
		+ "," + part.getWeight()
		+ "," + part.getUpdateSerialWithCount()
		+ "," + part.getPackTypeId()
		+ "," + part.getMiniStock()
		+ "," + part.getImportanta()
		+ "," + part.getHighValue()
		+ "," + part.getDecantChargeId()
		+ "," + part.getDespatchChargeId()
		+ "," + part.getInspectChargeId()
		+ "," + part.getReceiptChargeId()
		+ "," + part.getStorageChargeId()
		+ "," + part.getTransportChargeId()
		+",'" + part.getCreatedBy()+"'"
		+ "," + part.getAutoPick()
		+ "," + part.getAutoReceive()
		+",'" +  part.getJisSupplyGroup()+"'"
		+ "," + part.getPiecePartNumber()
		+",'"+ part.getVendorName()+"'"
		+ "," + part.getReplenPriority()
		+ ");";
		
		
		System.out.println(sqlText);
	}

	// SQL to drop Table
	private static void dropTable(Connection conn) {
		try {
			Statement st = conn.createStatement();
			st.executeUpdate("DROP TABLE IF EXISTS vantec.E2PART");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void copyFile(String from, String to) throws IOException {
		Path src = Paths.get(from);
		Path dest = Paths.get(to);
		Files.copy(src, dest, StandardCopyOption.REPLACE_EXISTING);
	}

	// Read IDOC Map & Store Mapping Values (Segment Id., Field Length, Data
	// Type)
	private static List<idocMap> getIdocMap(String fileName) {
		ArrayList<idocMap> map = new ArrayList<idocMap>();
		try {
			File file = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {

				idocMap idocMap = new idocMap();
				idocMap.segment = line.split(":")[0];
				idocMap.length = line.split(":")[1];
				idocMap.map = line.split(":")[2];

				map.add(idocMap);

			}
			br.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		return map;
	}

	// SQL to Drop Table
	public static void sqlDropTable(Connection conn, String fileName) {
		String sqlText = "DROP TABLE IF EXISTS vantec." + fileName;

		try {
			Statement st = conn.createStatement();
			st.executeUpdate(sqlText);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// SQL to Create Table
	private static void sqlCreateTable(Connection conn, String fileName, String[] detailArray) {
		int d = 0;
		String sqlText = "CREATE TABLE IF NOT EXISTS vantec." + fileName
				+ " (id bigint AUTO_INCREMENT PRIMARY KEY, version bigint, date_created timestamp, uniqueid varchar(20)";
		while (d < detailArray.length) {
			if (detailArray[d + 3].length() > 0) {
				sqlText = sqlText.trim() + "," + detailArray[d] + " " + detailArray[d + 3];
			}
			d = d + 4;
		}
		sqlText = sqlText.trim() + ")";
		try {
			Statement st = conn.createStatement();
			st.execute(sqlText);
		} catch (Exception e) {
		}
	}

	private static void writeXMLFile(Connection conn) {
		List<idocMap> map = new ArrayList<idocMap>();
		String detailArray[];
		String lineSeg = "";
		String lineStr = "";
		String lineTxt = "";
		String detailStr = "";
		String segmentStr = "";
		String segment = "";
		String sqlStr = "";
		String sqlText = "";
		String sqlCrtText = "";
		String uniqueId = "";

		int d = 0;
		int offset = 0;
		int strPos = 0;
		int lastPos = 0;
		try {
			new File("C:\\RRMCJobs\\PartUpdate\\IDOCINPartOut.txt").delete();
			new File("C:\\RRMCJobs\\PartUpdate\\IDOCINPartOut.xml").delete();
			// Open File for Writing Formatted IDOCS data
			FileWriter wf = new FileWriter("C:\\RRMCJobs\\PartUpdate\\IDOCINPartOut.txt");
			FileWriter wfxml = new FileWriter("C:\\RRMCJobs\\PartUpdate\\IDOCINPartOut.XML");

			// write xml Header
			wfxml.write("<WMTORD>" + "\r\n");
			wfxml.write("<IDOC BEGIN = \"1\">" + "\r\n");

			// Read IDOC Map & Store Mapping Values
			map = getIdocMap("C:\\RRMCJobs\\PartUpdate\\IDOCMapIn.txt");
			System.out.println("MAP done");

			// Read IDOC Transaction Data & parse data based on Mapping Array
			File file = new File("C:\\RRMCJobs\\PartUpdate\\ZIF_LSP_MM.OUT");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {

				lineSeg = line.trim();
				lineTxt = line;
				// System.out.println("Line is @ "+lineTxt);

				// Strip out Break Characters
				lineTxt = lineTxt.replaceAll("\t", "");
				lineTxt = lineTxt.replaceAll("\n", "");
				lineTxt = lineTxt.replaceAll("\r", "");
				lineTxt = lineTxt.replaceAll("'", " ");
				lineTxt = lineTxt.replaceAll(",", " ");
				lineTxt = lineTxt.replaceAll("#", " ");
				lineTxt = lineTxt.replaceAll("\"", " ");

				// Store TimeStamp
				uniqueId = Long.toString(System.currentTimeMillis());

				// set initial line offset value
				offset = 0;

				if (lineSeg.length() > 10) {
					// loop until end of Line is reached
					while (true) {
						if (offset >= lineTxt.length())
							break;
						// Parse IDOC Segment Id
						segment = "E2PART";
						if ((segment.trim().equals("")) || (segment.length() == 0))
							break;
						// Write XML Segment
						wfxml.append("<" + segment.trim() + " SEGMENT = \"1\">" + "\r\n");
						segmentStr = "";
						// try to find matching segment map in segment array
						for (idocMap idoc : map) {

							detailArray = idoc.toString().split(":");
							if (segment.trim().equals(detailArray[0].trim())) {

								try {
									lineStr = lineTxt.substring(offset, offset + Integer.parseInt(detailArray[1]));
								} catch (Exception ex) {
									lineStr = "";
								}
								sqlStr = "";
								// Split out Segment Field Map
								// (Name,Start,Length)
								detailArray = detailArray[2].split(";");
								d = 0;
								// Delete / Create SQL TABLE
								if (sqlCrtText.equals("")) {
									sqlCrtText = "CREATE TABLE IF NOT EXISTS vantec." + segment
											+ " (id bigint AUTO_INCREMENT PRIMARY KEY, version bigint, date_created timestamp, uniqueid varchar(20)";
									while (d < detailArray.length) {
										if (detailArray[d + 3].length() > 0) {
											sqlCrtText = sqlCrtText.trim() + "," + detailArray[d] + " "
													+ detailArray[d + 3];
										}
										d = d + 4;
									}
									sqlCrtText = sqlCrtText.trim() + ")";
									try {
										Statement st = conn.createStatement();
										st.executeUpdate(sqlCrtText);
										System.out.println("Table created");
									} catch (SQLException ex) {
										System.out.println("ERROR: " + ex);
									}
								}
								d = 0;
								while (d < detailArray.length) {
									if (detailArray[d + 1].equals("+"))
										strPos = lastPos;
									else
										strPos = Integer.parseInt(detailArray[d + 1]);
									// Format Output String
									if (lineStr.length() > strPos) {
										if (lineStr.length() >= strPos + Integer.parseInt(detailArray[d + 2])) {
											detailStr = detailArray[d] + ":" + lineStr.substring(strPos,
													strPos + Integer.parseInt(detailArray[d + 2]));
											wfxml.write("<" + detailArray[d].trim() + ">"
													+ lineStr.substring(strPos,
															strPos + Integer.parseInt(detailArray[d + 2]))
													+ "</" + detailArray[d].trim() + ">" + "\r\n");
											if (detailArray[d + 3].length() > 0) {
												if (sqlStr.length() > 0) {
													sqlStr = sqlStr.trim() + ",";
												}
												if ((detailArray[d + 3].length() > 6)
														&& (detailArray[d + 3].substring(0, 7).equals("varchar"))) {
													sqlStr = sqlStr.trim() + "'"
															+ (lineStr
																	.substring(strPos,
																			strPos + Integer
																					.parseInt(detailArray[d + 2]))
																	.trim() + "'");
												} else {
													sqlStr = sqlStr.trim() + (lineStr.substring(strPos,
															strPos + Integer.parseInt(detailArray[d + 2]))).trim();
												}
											}
										} else {
											detailStr = detailArray[d] + ":"
													+ lineStr.substring(strPos, lineStr.length());
											wfxml.write("<" + detailArray[d].trim() + ">"
													+ lineStr.substring(strPos, lineStr.length()) + "</"
													+ detailArray[d].trim() + ">" + "\r\n");
											if (detailArray[d + 3].length() > 0) {
												if (sqlStr.length() > 0) {
													sqlStr = sqlStr.trim() + ",";
												}
												if ((detailArray[d + 3].length() > 6)
														&& (detailArray[d + 3].substring(0, 7).equals("varchar"))) {
													sqlStr = sqlStr.trim() + "'"
															+ (lineStr.substring(strPos, lineStr.length())).trim()
															+ "'";
												} else {
													sqlStr = sqlStr.trim()
															+ (lineStr.substring(strPos, lineStr.length())).trim();
												}
											}
										}

										if (segmentStr.length() > 0)
											segmentStr = segmentStr.trim() + ",";
										segmentStr = segmentStr + detailStr;

										lastPos = strPos + Integer.parseInt(detailArray[d + 2]);
									}

									d = d + 4;
								}

								// Write Output String to File
								if (!segmentStr.equals("")) {
									wf.write(segmentStr.trim() + "\r\n");
									wfxml.write("</" + segment.trim() + ">" + "\r\n");
									if (!segment.equals("EDI_DC4")) {
										sqlText = "INSERT INTO vantec." + segment.trim() + " VALUES(DEFAULT,0,now(),'"
												+ uniqueId + "'," + sqlStr.trim() + ")";
										try {
											Statement st = conn.createStatement();
											st.executeUpdate(sqlText);
										} catch (SQLException ex) {
											System.out.println("ERROR: ");
										}
									}

								}

								// Increment Line Offset
								detailArray = idoc.toString().split(":");
								offset = offset + Integer.parseInt(detailArray[1]);
							}
						}
						if ((segmentStr.equals("")) || (segmentStr.length() == 0))
							break;
					}
				}
			}

			// Write XML Footer
			wfxml.append("</IDOC>" + "\r\n");
			wfxml.append("</WMTORD>" + "\r\n");
			wfxml.close();
			wf.close();
			br.close();

		} catch (IOException ex) {

		}
	}

	// SQL to Insert Values
	private static void sqlInsertData(Connection conn, String fileName, String sqlStr, String uniqueId) {

		String sqlText = "INSERT INTO vantec." + fileName.trim() + " VALUES(DEFAULT,0,now(),'" + uniqueId + "',"
				+ sqlStr.trim() + ")";
		System.out.println("uniqueId " + uniqueId);

		try {
			Statement st = conn.createStatement();
			st.execute(sqlText);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void partInsert(Connection conn) {
		try {

			String sqlStr = "";
			String sqlText = "";
			String MATNR = "";
			String DESC = "";
			String UOM = "";
			String PART_FAMILY = "";
			String VENDOR_PART_CODE = "";
			String JIS_TYPE = "";

			// Main Process

			sqlStr = "SELECT a.*,b.id as VendorId FROM vantec.E2PART a LEFT JOIN vantec.vendor b on a.VENDOR = b.vendor_reference_code WHERE NOT EXISTS(SELECT part_number FROM vantec.part where (MATNR=part_number))";

			//System.out.println(sqlStr);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				DESC = rs.getString("DESC_ENGLISH");
				MATNR = rs.getString("MATNR").trim();
				if (MATNR.length() > 10) {
					MATNR = MATNR.substring(0, 10);
				}

				if (DESC != null) {
					if (DESC.length() > 30) {
						DESC = DESC.substring(0, 30);
					}
				}

				UOM = rs.getString("UOM");
				if (UOM == null)
					UOM = "";

				PART_FAMILY = rs.getString("PART_FAMILY");
				if (PART_FAMILY == null)
					PART_FAMILY = "";

				VENDOR_PART_CODE = rs.getString("VENDOR_PART_CODE");
				if (VENDOR_PART_CODE == null)
					VENDOR_PART_CODE = "";

				JIS_TYPE = rs.getString("JIS_TYPE");
				if (JIS_TYPE == null)
					JIS_TYPE = "";

				Part part = new Part();
				part.setId("DEFAULT");
				part.setVersion("0");
				part.setDateCreated("now()");
				part.setEffectiveFrom("now()");
				part.setEffectiveTo("2035-12-31 23:59:59");
				part.setFixedLocationId("null");
				part.setFixedLocationTypeId("null");
				part.setFixedLocationSubTypeId("null");
				part.setImaginaryPart("0");
				part.setLastUpdated("now()");
				part.setLastUpdatedBy("ADDN");
				part.setPartDescription(DESC);
				part.setPartNumber(MATNR);
				part.setRequiresCount("0");
				part.setRequiresDecant("0");
				part.setRequiresInspection("0");
				part.setSafetyStock("0");
				part.setShelfLife("0");
				part.setVendorId("1");
				part.setVendorPartCode("");
				part.setWiCode("A1");
				part.setConversionFactor("1000");
				part.setFullBoxPick("0");
				part.setActive("1");
				part.setRdtPrompt("");
				part.setJisType(JIS_TYPE);
				part.setModular("0");
				part.setPartFamily(PART_FAMILY);
				part.setUom(UOM);
				part.setIncludeInventory("0");
				part.setLoosePartQty("0");
				part.setMaxPickFaceQty("0");
				part.setWeight("0");
				part.setUpdateSerialWithCount("0");
				part.setPackTypeId("null");
				part.setMiniStock("0");
				part.setImportanta("0");
				part.setHighValue("0");
				part.setDecantChargeId("null");
				part.setDespatchChargeId("null");
				part.setInspectChargeId("null");
				part.setReceiptChargeId("null");
				part.setStorageChargeId("null");
				part.setTransportChargeId("null");
				part.setCreatedBy("ADDN");
				part.setAutoPick("0");
				part.setAutoReceive("0");
				part.setJisSupplyGroup(VENDOR_PART_CODE);
				part.setPiecePartNumber("null");
				part.setVendorName("");
				part.setReplenPriority("null");

				/*sqlText = ("INSERT INTO vantec.part VALUES(DEFAULT,0,now(),now(),'2035-12-31 23:59:59',null,null,null,0,now(),'ADDN','"
						+ DESC + "','" + MATNR + "',0,0,0,0,0,866,'" + VENDOR_PART_CODE + "','A1',1000,0,1,'','"
						+ JIS_TYPE + "',0,'" + PART_FAMILY + "','" + UOM
						+ "',0,0,0,0,0,null,0,0,0,null,null,null,null,null,null,'ADDN',0,0,null)");
				*/
				
				
				sqlText = "INSERT INTO vantec.part VALUES(" 
						+ part.getId() 
						+ "," + part.getVersion()
						+ "," +  part.getDateCreated()
						+ "," +  part.getEffectiveFrom()
						+ ",'" + part.getEffectiveTo() + "'"
						+ "," +  part.getFixedLocationId()
						+ "," +  part.getFixedLocationTypeId()
						+ "," + part.getFixedLocationSubTypeId()
						+ "," + part.getImaginaryPart()
						+ "," + part.getLastUpdated()
						+ ",'" + part.getLastUpdatedBy() + "'"
						+ ",'" + part.getPartDescription() + "'"
						+ ",'"  + part.getPartNumber() + "'"
						+ "," + part.getRequiresCount() 
						+ "," + part.getRequiresDecant()
						+ "," + part.getRequiresInspection()
						+ "," + part.getSafetyStock()
						+ "," + part.getShelfLife()
						+ "," + part.getVendorId()
						+ ",'"+ part.getVendorPartCode() + "'"
						+ ",'"+ part.getWiCode() + "'" 
						+ "," + part.getConversionFactor()
						+ "," + part.getFullBoxPick()
						+ "," + part.getActive()
						+ ",'"+ part.getRdtPrompt()+"'"
						+",'"+ part.getJisType()+"'"
						+ "," + part.getModular()
						+",'"+ part.getPartFamily()+"'"
						+",'" + part.getUom()+"'"
						+ "," + part.getIncludeInventory()
						+ "," + part.getLoosePartQty()
						+ "," + part.getMaxPickFaceQty()
						+ "," + part.getWeight()
						+ "," + part.getUpdateSerialWithCount()
						+ "," + part.getPackTypeId()
						+ "," + part.getMiniStock()
						+ "," + part.getImportanta()
						+ "," + part.getHighValue()
						+ "," + part.getDecantChargeId()
						+ "," + part.getDespatchChargeId()
						+ "," + part.getInspectChargeId()
						+ "," + part.getReceiptChargeId()
						+ "," + part.getStorageChargeId()
						+ "," + part.getTransportChargeId()
						+",'" + part.getCreatedBy()+"'"
						+ "," + part.getAutoPick()
						+ "," + part.getAutoReceive()
						+",'" +  part.getJisSupplyGroup()+"'"
						+ "," + part.getPiecePartNumber()
						+",'"+ part.getVendorName()+"'"
						+ "," + part.getReplenPriority()
						+ ");";
				
				
				Statement createSt = conn.createStatement();
				createSt.executeUpdate(sqlText);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void partUpdateQA(Connection conn) {

		String sqlStr = "";
		String sqlText = "";

		try{
			sqlStr = "SELECT a.*,b.id as VendorId FROM  vantec.E2PART a LEFT JOIN vantec.vendor b on a.VENDOR = b.vendor_reference_code";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				try {
					if (rs.getString("QUAL_INS").equals("X")) {
						sqlText = ("UPDATE vantec.part SET last_updated = now(),last_updated_by = 'INSP',requires_inspection = 1, include_inventory=0 where requires_inspection = 0 and part_number='"
								+ rs.getString("MATNR") + "'");
					} else {
						sqlText = ("UPDATE vantec.part SET last_updated = now(),last_updated_by = 'INSP',requires_inspection = 0, include_inventory=0 where requires_inspection = 1 and part_number='"
								+ rs.getString("MATNR") + "'");
					}
					Statement createSt = conn.createStatement();
					createSt.executeUpdate(sqlText);
				} catch (SQLException se) {
					System.out.println("Error " + se);
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}

	private static void partUpdateVendorPart(Connection conn) {

		try {
			String sqlStr = "";
			String sqlText = "";

			sqlStr = "SELECT a.*,b.id as VendorId FROM vantec.E2PART a LEFT JOIN vantec.vendor b on a.VENDOR = b.vendor_reference_code WHERE a.vendor_part_code <> ''";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				try{
				if (!rs.getString("vendor_part_code").equals("")) {
					sqlText = ("UPDATE vantec.part SET last_updated = now(),last_updated_by = 'SQL', vendor_part_code='"
							+ rs.getString("vendor_part_code") + "', jis_supply_group='"+ rs.getString("vendor_part_code") + "', jis_type='" + rs.getString("jis_type")
							+ "', part_family='" + rs.getString("part_family") + "' WHERE part_number='"
							+ rs.getString("MATNR") + "' AND jis_supply_group <> '" + rs.getString("vendor_part_code")
							+ "'");
				}
				Statement updateStatement = conn.createStatement();
				updateStatement.executeUpdate(sqlText);
				}catch(SQLException se){
					se.printStackTrace();
				}
			}
		} catch (SQLException se) {
			se.printStackTrace();

		}
	}

	private static void partUpdateLocationType(Connection conn) {

		try {
			String sqlStr = "";
			String sqlText = "";

			sqlStr = "SELECT a.*,b.fixed_location_type_id as locationTypeId, b.fixed_sub_type_id AS locationSubTypeId FROM vantec.E2PART a JOIN vantec.PART b on a.MATNR = b.part_number WHERE b.fixed_location_type_id IS NOT NULL OR b.fixed_sub_type_id IS NOT NULL";
			
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			Statement createSt = conn.createStatement();
			while (rs.next()) {
				try {
					if (rs.getInt("locationTypeId") != 0) {
						sqlText = ("UPDATE vantec.part SET last_updated = now(),last_updated_by = 'LOCATIONTYPE', fixed_location_type_id="
								+ rs.getInt("locationTypeId") + " WHERE substr(part_number,1,7)=substr('"
								+ rs.getString("MATNR") + "',1,7) AND fixed_location_type_id IS NULL");
						//System.out.println(sqlText);
						createSt.executeUpdate(sqlText);

					}
					if (rs.getInt("locationSubTypeId") != 0) {
						sqlText = ("UPDATE vantec.part SET last_updated = now(),last_updated_by = 'LOCATIONTYPE', fixed_sub_type_id="
								+ rs.getInt("locationSubTypeId") + " WHERE substr(part_number,1,7)=substr('"
								+ rs.getString("MATNR") + "',1,7) AND fixed_sub_type_id IS NULL");
						createSt.executeUpdate(sqlText);
					}
				}catch(SQLException se){
					se.printStackTrace();
				}
			}
			sqlText = "UPDATE vantec.part SET last_updated = now(),last_updated_by = 'LOCATIONTYPE', fixed_location_type_id=49 , fixed_sub_type_id=268 where date(date_created) = curdate() and fixed_location_type_id IS NULL and fixed_sub_type_id IS NULL; ";
			createSt.executeUpdate(sqlText);

		} catch (SQLException se) {
			se.printStackTrace();

		}
	}

}


class idocMap {
	String segment;
	String length;
	String map;

	public String toString() {
		return segment + ":" + length + ":" + map;
	}
}
