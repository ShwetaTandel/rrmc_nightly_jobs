package com.vantec.jobs;

import java.util.Date;

public class Part {
	
	private String id; 
	private String version;
	private String dateCreated;
	private String effectiveFrom;
	private String effectiveTo;
	private String fixedLocationId;
	private String fixedLocationTypeId;
	private String fixedLocationSubTypeId;
	private String imaginaryPart;
	private  String lastUpdated;
	private String lastUpdatedBy;
	private String partDescription;
	private String partNumber;
	private String requiresCount;
	private String requiresDecant;
	private String requiresInspection;
	private String safetyStock;
	private String shelfLife;
	private String vendorId;
	private String vendorPartCode;
	private String wiCode;
	private String conversionFactor;
	private String fullBoxPick;
	private String active;
	private String rdtPrompt;
	private String jisType;
	private String modular;
	private String partFamily;
	private String uom;
	private String includeInventory;
	private String loosePartQty;
	private String maxPickFaceQty;
	private String weight;
	private String updateSerialWithCount;
	private String packTypeId;
	private String miniStock;
	private String importanta;
	private String highValue;
	private String decantChargeId;
	private String despatchChargeId;
	private String inspectChargeId;
	private String receiptChargeId;
	private String storageChargeId;
	private String transportChargeId;
	private String createdBy;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	public String getEffectiveTo() {
		return effectiveTo;
	}
	public void setEffectiveTo(String effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public String getFixedLocationId() {
		return fixedLocationId;
	}
	public void setFixedLocationId(String fixedLocationId) {
		this.fixedLocationId = fixedLocationId;
	}
	public String getFixedLocationTypeId() {
		return fixedLocationTypeId;
	}
	public void setFixedLocationTypeId(String fixedLocationTypeId) {
		this.fixedLocationTypeId = fixedLocationTypeId;
	}
	public String getFixedLocationSubTypeId() {
		return fixedLocationSubTypeId;
	}
	public void setFixedLocationSubTypeId(String fixedLocationSubTypeId) {
		this.fixedLocationSubTypeId = fixedLocationSubTypeId;
	}
	public String getImaginaryPart() {
		return imaginaryPart;
	}
	public void setImaginaryPart(String imaginaryPart) {
		this.imaginaryPart = imaginaryPart;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getRequiresCount() {
		return requiresCount;
	}
	public void setRequiresCount(String requiresCount) {
		this.requiresCount = requiresCount;
	}
	public String getRequiresDecant() {
		return requiresDecant;
	}
	public void setRequiresDecant(String requiresDecant) {
		this.requiresDecant = requiresDecant;
	}
	public String getRequiresInspection() {
		return requiresInspection;
	}
	public void setRequiresInspection(String requiresInspection) {
		this.requiresInspection = requiresInspection;
	}
	public String getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(String safetyStock) {
		this.safetyStock = safetyStock;
	}
	public String getShelfLife() {
		return shelfLife;
	}
	public void setShelfLife(String shelfLife) {
		this.shelfLife = shelfLife;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorPartCode() {
		return vendorPartCode;
	}
	public void setVendorPartCode(String vendorPartCode) {
		this.vendorPartCode = vendorPartCode;
	}
	public String getWiCode() {
		return wiCode;
	}
	public void setWiCode(String wiCode) {
		this.wiCode = wiCode;
	}
	public String getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(String conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public String getFullBoxPick() {
		return fullBoxPick;
	}
	public void setFullBoxPick(String fullBoxPick) {
		this.fullBoxPick = fullBoxPick;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getRdtPrompt() {
		return rdtPrompt;
	}
	public void setRdtPrompt(String rdtPrompt) {
		this.rdtPrompt = rdtPrompt;
	}
	public String getJisType() {
		return jisType;
	}
	public void setJisType(String jisType) {
		this.jisType = jisType;
	}
	public String getModular() {
		return modular;
	}
	public void setModular(String modular) {
		this.modular = modular;
	}
	public String getPartFamily() {
		return partFamily;
	}
	public void setPartFamily(String partFamily) {
		this.partFamily = partFamily;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getIncludeInventory() {
		return includeInventory;
	}
	public void setIncludeInventory(String includeInventory) {
		this.includeInventory = includeInventory;
	}
	public String getLoosePartQty() {
		return loosePartQty;
	}
	public void setLoosePartQty(String loosePartQty) {
		this.loosePartQty = loosePartQty;
	}
	public String getMaxPickFaceQty() {
		return maxPickFaceQty;
	}
	public void setMaxPickFaceQty(String maxPickFaceQty) {
		this.maxPickFaceQty = maxPickFaceQty;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getUpdateSerialWithCount() {
		return updateSerialWithCount;
	}
	public void setUpdateSerialWithCount(String updateSerialWithCount) {
		this.updateSerialWithCount = updateSerialWithCount;
	}
	public String getPackTypeId() {
		return packTypeId;
	}
	public void setPackTypeId(String packTypeId) {
		this.packTypeId = packTypeId;
	}
	public String getMiniStock() {
		return miniStock;
	}
	public void setMiniStock(String miniStock) {
		this.miniStock = miniStock;
	}
	public String getImportanta() {
		return importanta;
	}
	public void setImportanta(String importanta) {
		this.importanta = importanta;
	}
	public String getHighValue() {
		return highValue;
	}
	public void setHighValue(String highValue) {
		this.highValue = highValue;
	}
	public String getDecantChargeId() {
		return decantChargeId;
	}
	public void setDecantChargeId(String decantChargeId) {
		this.decantChargeId = decantChargeId;
	}
	public String getDespatchChargeId() {
		return despatchChargeId;
	}
	public void setDespatchChargeId(String despatchChargeId) {
		this.despatchChargeId = despatchChargeId;
	}
	public String getInspectChargeId() {
		return inspectChargeId;
	}
	public void setInspectChargeId(String inspectChargeId) {
		this.inspectChargeId = inspectChargeId;
	}
	public String getReceiptChargeId() {
		return receiptChargeId;
	}
	public void setReceiptChargeId(String receiptChargeId) {
		this.receiptChargeId = receiptChargeId;
	}
	public String getStorageChargeId() {
		return storageChargeId;
	}
	public void setStorageChargeId(String storageChargeId) {
		this.storageChargeId = storageChargeId;
	}
	public String getTransportChargeId() {
		return transportChargeId;
	}
	public void setTransportChargeId(String transportChargeId) {
		this.transportChargeId = transportChargeId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getAutoPick() {
		return autoPick;
	}
	public void setAutoPick(String autoPick) {
		this.autoPick = autoPick;
	}
	public String getAutoReceive() {
		return autoReceive;
	}
	public void setAutoReceive(String autoReceive) {
		this.autoReceive = autoReceive;
	}
	public String getJisSupplyGroup() {
		return jisSupplyGroup;
	}
	public void setJisSupplyGroup(String jisSupplyGroup) {
		this.jisSupplyGroup = jisSupplyGroup;
	}
	public String getPiecePartNumber() {
		return piecePartNumber;
	}
	public void setPiecePartNumber(String piecePartNumber) {
		this.piecePartNumber = piecePartNumber;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getReplenPriority() {
		return replenPriority;
	}
	public void setReplenPriority(String replenPriority) {
		this.replenPriority = replenPriority;
	}
	private String autoPick;
	private String autoReceive;
	private String jisSupplyGroup;
	private String piecePartNumber;
	private String vendorName;
	private String replenPriority;
	
	
	
	
	
}
