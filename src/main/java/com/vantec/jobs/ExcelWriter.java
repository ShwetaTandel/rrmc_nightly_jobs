package com.vantec.jobs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.vantec.job.helper.celgard_weekly_report_helper;

public class ExcelWriter {
	
	public static String path="C:\\reports\\CelgardEmailJob\\";
	
	 
	 


	public static String write_celguard_weekly_Report(Connection conn) throws SQLException, IOException {
		
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("transaction");
        
        Row rowH = sheet.createRow(3);
        Cell cellH = rowH.createCell(5);
        cellH.setCellValue("Receipts & Deliveries");
        
        
        int rowCount = 7;
        Row row = sheet.createRow(rowCount);
        Cell cell = row.createCell(1);
        cell.setCellValue("Part Number");
        cell = row.createCell(2);
        cell.setCellValue("Reference");
        cell = row.createCell(3);
       // cell.setCellValue("Previous Balance");
       // cell = row.createCell(4);
        cell.setCellValue("Received");
        cell = row.createCell(4);
        cell.setCellValue("Delivered");
        cell = row.createCell(5);
        cell.setCellValue("Rolling Piece");
        cell = row.createCell(6);
        cell.setCellValue("Rolling Pallet");
        cell = row.createCell(7);
        //cell.setCellValue("Current Inventory");
        //cell = row.createCell(8);
        cell.setCellValue("Del Date");
        cell = row.createCell(8);
        cell.setCellValue("Slot");
        cell = row.createCell(9);
        cell.setCellValue("Ran");
        cell = row.createCell(10);
        cell.setCellValue("Serial");
        cell = row.createCell(11);
        cell.setCellValue("Hold");
        cell = row.createCell(12);
       
            
            //detail data
            ResultSet resultDetail = celgard_weekly_report_helper.celgard_getDetail(conn,"299F05SH3A");
            
            int rollingPiece = 0;
            int rollingPallet = 0;
            while (resultDetail.next()) { 
            	 int columnCountD = 0;
            	
        	row = sheet.createRow(++rowCount);
            String part_numberD = resultDetail.getString("part_number");
            Cell cellD = row.createCell(++columnCountD);
            cellD.setCellValue(part_numberD);
            String our_refD = resultDetail.getString("haulier_reference");
            String customer_referenceD = resultDetail.getString("customer_reference");
            cellD = row.createCell(++columnCountD);
            if(our_refD!=null){
            	cellD.setCellValue(our_refD);
            }else{
            	cellD.setCellValue(customer_referenceD);
            }
            
            //cellD = row.createCell(++columnCountD);
            //cellD.setCellValue("");
            String shortCodeD = resultDetail.getString("short_code");
            if(shortCodeD.startsWith("R")){
            	String txnQty = resultDetail.getString("scanned_qty");
            	cellD = row.createCell(++columnCountD);
            	cellD.setCellValue(txnQty);
            	int intTxn=(int)Double.parseDouble(txnQty);
            	rollingPiece = rollingPiece + intTxn;
            	rollingPallet = rollingPallet + 1;
            }else{
            	String txn_qty = "0";
            	cellD = row.createCell(++columnCountD);
            	cellD.setCellValue(txn_qty);
            	
            }
            if(shortCodeD.startsWith("P") || shortCodeD.startsWith("S")){
            	String txn_qty = resultDetail.getString("scanned_qty");
            	cellD = row.createCell(++columnCountD);
            	cellD.setCellValue(txn_qty);
            	int intTxn=(int)Double.parseDouble(txn_qty);
            	rollingPiece = rollingPiece - intTxn;
            	rollingPallet = rollingPallet - 1;
            }else{
            	String txn_qty = "0";
            	cellD = row.createCell(++columnCountD);
            	cellD.setCellValue(txn_qty);
            }
            
            //Rolling Piece
            cellD = row.createCell(++columnCountD);
            cellD.setCellValue(rollingPiece);
            //Rolling Pallet
            cellD = row.createCell(++columnCountD);
            cellD.setCellValue(rollingPallet);
            
          
            //cellD = row.createCell(++columnCountD);
            //cellD.setCellValue("");
            
           
            String date_createdD = resultDetail.getString("date_created");
            cellD = row.createCell(++columnCountD);
            cellD.setCellValue(date_createdD);
           
            
            cellD = row.createCell(++columnCountD);
            if(shortCodeD.equalsIgnoreCase("SA-OUT") || shortCodeD.equalsIgnoreCase("SA-IN")){
            	cellD.setCellValue(shortCodeD);
            }else{
            	cellD.setCellValue("");
            }
            
            String ran_or_orderD = resultDetail.getString("ran_order");
            cellD = row.createCell(++columnCountD);
            cellD.setCellValue(ran_or_orderD);
            String serial_referenceD = resultDetail.getString("serial_reference");
            cellD = row.createCell(++columnCountD);
            cellD.setCellValue(serial_referenceD);
            
            String status = celgard_weekly_report_helper.getStatus(conn,part_numberD, serial_referenceD);
           
            if(status!=null && status.equalsIgnoreCase("HOLD")){
            	cellD = row.createCell(++columnCountD);
                cellD.setCellValue(status);
            }else{
            	cellD = row.createCell(++columnCountD);
                cellD.setCellValue("");
            }
            
            }
            
        

        
        
        String filename = "celgard_weekly_report.xlsx";  
        FileOutputStream outputStream = new FileOutputStream(path+filename);  
        workbook.write(outputStream);
        workbook.close();
        return filename;  
	}

}


