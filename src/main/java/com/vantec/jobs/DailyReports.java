package com.vantec.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailyReports {

	public static void callDailyReports() {

		// copy file ZIF_LSP_STOCK.OUT from shared cddata to local path
		try {
			System.out.println("Get DB Connection...");
			Connection conn = JobsHelper.getConnection();
			System.out.println("Get DB Connection...");
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			List<String> fileList = new ArrayList<String>();
			System.out.println("Writing CSV inventory..");
			writeInventoryCSV(conn,"C:\\RRMCJobs\\DailyReport\\INVENTORYREPORT.CSV");
			System.out.println("Writing CSV carset..");
			writeCarsetPickCSV(conn,"C:\\RRMCJobs\\DailyReport\\CARSETPICKREPORT.CSV");
			fileList.add("C:\\RRMCJobs\\DailyReport\\INVENTORYREPORT.CSV");
			fileList.add("C:\\RRMCJobs\\DailyReport\\CARSETPICKREPORT.CSV");
			System.out.println("Sending emails.....");
			JobsHelper.sendMail("shweta-tandel.ce@vantec-gl.com", fileList,
					"RRMC Inventory " + sdf.format(dt));
			JobsHelper.sendMail(JobsHelper.getEmailsForJob("DAILY_REPORT", conn), fileList,
					"RRMC Carset Pick " + sdf.format(dt));
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	// Inventory to CSV format
	public static void writeInventoryCSV(Connection conn, String fileName){
		String sqlStr = "";
		String lineStr = "";
		try {
			new File(fileName).delete(); 
			FileWriter wf= new FileWriter(new File(fileName));
			lineStr = "Part_Number,Description,serial_reference,location_code,inventory_qty,allocated_qty,inventory_status_code,tag_reference,stock_date"+"\r\n";

			wf.write(lineStr);
			sqlStr = "SELECT a.part_id, a.serial_reference AS serial_reference, b.part_number AS part_number, b.part_description AS part_description, b.conversion_factor AS conversion_factor, a.inventory_qty AS inventory_qty, a.allocated_qty AS allocated_qty, a.parent_tag_id AS parent_tag_id, substring(a.stock_date,1,10) AS stock_date, c.location_code AS location_code, c.location_type_id AS location_type_id, c.current_location_status_id AS current_location_status_id, d.tag_reference as tag_reference, e.inventory_status_code AS inventory_status_code FROM vantec.inventory_master a LEFT JOIN vantec.part b ON a.part_id=b.id LEFT JOIN vantec.location c ON a.current_location_id = c.id LEFT JOIN vantec.tag d ON a.parent_tag_id=d.id LEFT JOIN vantec.inventory_status e ON c.current_location_status_id=e.id WHERE a.inventory_qty>0 ORDER BY a.part_id";
			Statement st = conn.createStatement();
			ResultSet rs  = st.executeQuery(sqlStr);
			while(rs.next()){
				String $mSerial_Reference = "";
				String $mTag_Reference = "";
				String $mPart_Description = "";
				int $mInventory_qty = 0;
				int $mAllocated_qty = 0;
				int $mConversion_factor = 0;
				if (rs.getInt("conversion_factor") == 0){
					$mConversion_factor=1000;
				} else{
					$mConversion_factor=rs.getInt("conversion_factor");
				}
				if (rs.getInt("inventory_qty") != 0)
					$mInventory_qty = rs.getInt("inventory_qty") / $mConversion_factor;
				if (rs.getInt("allocated_qty") != 0)
					$mAllocated_qty =rs.getInt("allocated_qty")/ $mConversion_factor;
				if (rs.getString("serial_reference") == null || rs.getString("serial_reference").equals("null")){
					$mSerial_Reference = "";
				}else{
					$mSerial_Reference = rs.getString("serial_reference");
				}
				// replace escape characters
				if ((rs.getString("part_description") == null)||  (rs.getString("part_description").equals("null")) ){
					$mPart_Description = "";
				}else{
					$mPart_Description = rs.getString("part_description");
				}
				$mPart_Description = $mPart_Description.replaceAll("\t","");
				$mPart_Description = $mPart_Description.replaceAll("\n","");
				$mPart_Description = $mPart_Description.replaceAll("\r","");
				$mPart_Description = $mPart_Description.replaceAll("'"," ");
				$mPart_Description = $mPart_Description.replaceAll(","," ");
				$mPart_Description = $mPart_Description.replaceAll("#"," ");
				$mPart_Description = $mPart_Description.replaceAll("\""," ");
				if ( (rs.getString("tag_reference") == null) || (rs.getString("tag_reference").equals("null")) ){
					$mTag_Reference = "";
				}else{
					$mTag_Reference = rs.getString("tag_reference");
				}

				lineStr = rs.getString("part_number")+","+$mPart_Description.trim()+","+$mSerial_Reference+","+rs.getString("location_code")+","+$mInventory_qty+","+$mAllocated_qty+","+rs.getString("inventory_status_code")+","+$mTag_Reference+","+rs.getDate("stock_date")+"\r\n";

				wf.write(lineStr);
			} 
			wf.close();
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException io){
			io.printStackTrace();
		}
	}
	
	// Carset Pick History to CSV format
	public static void writeCarsetPickCSV(Connection conn, String fileName)
	{
	  String sqlStr = "";
	  String lineStr = "";
	  
	  try{
	  new File(fileName).delete(); 
	  FileWriter wf= new FileWriter(new File(fileName));
	  lineStr = "id,associated_document_reference,date_created,from_location_code,part_number,ran_or_order,car_set,serial_reference,short_code,txn_qty,last_updated_by"+"\r\n";
	  
	  wf.write(lineStr);
	 
	 sqlStr = "SELECT a.id AS id, a.associated_document_reference AS associated_document_reference, substring(a.date_created,1,19) AS date_created, a.from_location_code AS from_location_code, a.part_number AS part_number, a.ran_or_order AS ran_or_order, a.comment AS comment, a.serial_reference AS serial_reference, a.short_code AS short_code, a.txn_qty AS txn_qty, a.last_updated_by AS last_updated_by, b.conversion_factor AS conversion_factor, c.benum AS benum FROM vantec.transaction_history a LEFT JOIN vantec.part b ON a.part_number=b.part_number LEFT JOIN vantec.document_header c ON a.ran_or_order=tanum WHERE a.short_code='CSET' and date(a.date_created) >=curdate() ORDER BY a.date_created";
	  Statement st = conn.createStatement();
	  ResultSet rs = st.executeQuery(sqlStr);
	  while(rs.next()){
		  String $mComment = "";
		  int $mTxn_qty = 0;
		  int $mConversion_factor = 0;
		  if (rs.getInt("conversion_factor") == 0){
				$mConversion_factor=1000;
		  }else{
				$mConversion_factor=rs.getInt("conversion_factor");
		  }
		  if (rs.getInt("txn_qty") != 0){
				$mTxn_qty = rs.getInt("txn_qty") / $mConversion_factor;
		  }
		  if (rs.getString("comment") == null){
				$mComment = rs.getString("benum");
		  } else{
				$mComment = rs.getString("comment");
		  }
		  
		lineStr = rs.getInt("id")+","+rs.getString("associated_document_reference")+","+rs.getDate("date_created")+","+rs.getString("from_location_code")+","+rs.getString("part_number")+","+rs.getString("ran_or_order")+","+$mComment+","+rs.getString("serial_reference")+","+rs.getString("short_code")+","+$mTxn_qty+","+rs.getString("last_updated_by")+"\r\n";
		


		wf.write(lineStr);
	  }
	  wf.close();
	  }catch(SQLException q){
		  q.printStackTrace();
	  }catch(IOException io){
		  io.printStackTrace();
	  }
	  
	}

}
