package com.vantec.jobs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class JobsHelper {
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://vimsrrmc:3306/vantec?useSSL=false", "root", "root");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	///Get email list for JON name
	public static String getEmailsForJob(String jobName, Connection conn){
		String response = "";
		String sqlStr = "SELECT distribution_list from vantec.job_definition WHERE job_name = '"+jobName+"';";
		Statement st;
		try {
			st = conn.createStatement();
			ResultSet rs  = st.executeQuery(sqlStr);
			while(rs.next()){
				response = rs.getString("distribution_list");
			
			}
		} catch (SQLException e) {
 			e.printStackTrace();
		}
		return response;
	}

	public static void sendMail(String toAddress, List<String> fileList, String subject) {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.auth", "false");
		properties.put("mail.smtp.host", "172.16.10.196");
		properties.put("mail.smtp.port", 25);
		Session session = Session.getDefaultInstance(properties);

		session = Session.getInstance(properties);

		try {

			// Compose the message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("accounts@vanteceurope.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
			message.setSubject(subject);
			message.setText("This was prepared by an automated job!!");

			// Attach attachments
			Multipart multipart = new MimeMultipart();
			for (String file : fileList) {
				MimeBodyPart messageBody = new MimeBodyPart();
				messageBody = new MimeBodyPart();
				DataSource source = new FileDataSource(file);
				messageBody.setDataHandler(new DataHandler(source));
				messageBody.setFileName(file);
				multipart.addBodyPart(messageBody);
			}

			// send the message
			message.setContent(multipart);
			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void copyFile(String from, String to) throws IOException {
		Path src = Paths.get(from);
		Path dest = Paths.get(to);
		Files.copy(src, dest, StandardCopyOption.REPLACE_EXISTING);
	}

}
