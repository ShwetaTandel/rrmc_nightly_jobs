package com.vantec.jobs;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Hello world!
 *
 */
public class SendEmail 
{
	
	   public static String path="C:\\reports\\CelgardEmailJob\\";
	
	   public static void sendMail(String fileName,ResultSet distributionListResult,String subject) throws IOException, SQLException {    
		       String to = null ;
		       while (distributionListResult.next()) { 
		    	   to = distributionListResult.getString("distribution_list");
		       }
		       // to = "tanushree-agrawal.ww@vantec-gl.com,sarah-mcmann.ak@vantec-gl.com";
		       String[] toList = to.split(",");
		       String from = "accounts@vantec-gl.com";
				
			   Properties properties = new Properties();
			   properties.put("mail.smtp.auth", "false");
			   properties.put("mail.smtp.host", "172.16.10.196");
			   properties.put("mail.smtp.port", "25");
			   
			      
			      Session session = Session.getDefaultInstance(properties);

			      try {
			         MimeMessage message = new MimeMessage(session);
			         message.setFrom(new InternetAddress(from));
					 for(int i=0;i<toList.length;i++){
						 System.out.println(toList[i]);
						 message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toList[i]));
					 }
			         message.setSubject(subject);
			         message.setText(subject);
			         
			         //attachment
				        
				        //DataSource fds = new FileDataSource("C:\\reports\\yazaki"+fileName);
			         DataSource fds = new FileDataSource(path+fileName);
				        // Create the message part
				        BodyPart messageBodyPart = new MimeBodyPart();

				        // Fill the message
				        messageBodyPart.setText(subject);

				        Multipart multipart = new MimeMultipart();
				        multipart.addBodyPart(messageBodyPart);

				        // Part two is attachment
				        messageBodyPart = new MimeBodyPart();
				        //DataSource source = new FileDataSource(filename);
				        messageBodyPart.setDataHandler(new DataHandler(fds));
				        messageBodyPart.setFileName(fileName);
				        multipart.addBodyPart(messageBodyPart);

				        // Put parts in message
				        message.setContent(multipart);

			         // Send message
			         Transport.send(message);
			         System.out.println("Sent message successfully....");
			      } catch (MessagingException mex) {
			         mex.printStackTrace();
			      }
			   }
		
}

