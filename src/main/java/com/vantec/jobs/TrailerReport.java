package com.vantec.jobs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrailerReport {
	public static void callTrailerReport() {

		// copy file ZIF_LSP_STOCK.OUT from shared cddata to local path
		try {
			System.out.println("Get DB Connection...");
			Connection conn = JobsHelper.getConnection();
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			List<String> fileList = new ArrayList<String>();
			System.out.println("Writing trailer CSV file");
			writeTrailerCSV(conn, "C:\\RRMCJobs\\TrailerReport\\TRAILERREPORT.CSV");
			fileList.add("C:\\RRMCJobs\\TrailerReport\\TRAILERREPORT.CSV");
			System.out.println("Sending emails............");
			JobsHelper.sendMail("shweta-tandel.ce@vantec-gl.com", fileList,
					"RRMC Trailer" + sdf.format(dt));
			JobsHelper.sendMail(JobsHelper.getEmailsForJob("TRAILER_REPORT", conn), fileList,
					"RRMC Trailer" + sdf.format(dt));
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	// Data to CSV format
	public static void writeTrailerCSV(Connection conn, String fileName) {
		String sqlStr = "";
		String lineStr = "";
		try {
			new File(fileName).delete();
			FileWriter wf = new FileWriter(new File(fileName));
			lineStr = "Third_Party_Ref.,Expected_Delivery_Date,Date_Created,Parent_Tag,Pick_To_Trailer_Date,Operator,To_Location,Pick_Type"
					+ "\r\n";

			wf.write(lineStr);

			// sqlStr = "SELECT a.customer_reference_code AS
			// customer_reference_code, substr(a.expected_delivery,1,19) AS
			// expected_delivery, b.parent_tag AS parent_tag, b.short_code AS
			// short_code FROM vantec.document_header a LEFT JOIN
			// vantec.transaction_history b ON a.customer_reference_code =
			// b.ran_or_order WHERE a.document_type = 'RRM Order' AND
			// SUBSTRING(a.expected_delivery,1,10) =
			// '"+timeStamp.substring(0,10)+"'";
			sqlStr = "SELECT a.customer_reference_code AS customer_reference_code, substr(a.expected_delivery,1,19) AS expected_delivery, substr(a.date_created,1,19) AS date_created, a.order_type AS order_type FROM vantec.document_header a WHERE a.document_type = 'RRM Order' AND date(a.expected_delivery) = SUBDATE(CURDATE(),1)";

			String $mOrder_Type = "";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sqlStr);
			while (rs.next()) {
				$mOrder_Type = rs.getString("order_type");
				if (rs.getString("order_type").equals("350"))
					$mOrder_Type = "350 - GLTP";
				if (rs.getString("order_type").equals("913"))
					$mOrder_Type = "913 - EMERG";
				if (rs.getString("order_type").equals("943"))
					$mOrder_Type = "943 - CSET";
				if (rs.getString("order_type").equals("945"))
					$mOrder_Type = "945 - CSET";
				if (rs.getString("order_type").equals("988"))
					$mOrder_Type = "988 - PC PICK";
				if (rs.getString("order_type").equals("993"))
					$mOrder_Type = "993 - PC PICK";

				String pickStr = "SELECT b.parent_tag AS parent_tag, b.short_code AS short_code FROM vantec.transaction_history b WHERE b.ran_or_order = '"
						+ rs.getString("customer_reference_code") + "' LIMIT 1";
				String $mParent_Tag = "";
				Statement stPick = conn.createStatement();
				ResultSet rsPick = stPick.executeQuery(pickStr);
				while (rsPick.next()) {
					if (rsPick.getString("parent_tag") != null) {
						$mParent_Tag = rsPick.getString("parent_tag");
					} else {
						$mParent_Tag = "";
					}
				}
				String $mLast_Updated = "";
				String $mLast_Updated_By = "";
				String $mTo_Location_Code = "";
				if ($mParent_Tag != "") {
					String trailerStr = "SELECT substr(c.last_updated,1,19) AS last_updated, c.last_updated_by AS last_updated_by, c.to_location_code AS to_location_code FROM vantec.transaction_history c WHERE c.short_code IN('PTT','CTT') AND c.source_tag='"
							+ $mParent_Tag + "' LIMIT 1";
					Statement stTrailer = conn.createStatement();
					ResultSet rsTrailer = stTrailer.executeQuery(trailerStr);
					while (rsTrailer.next()) {
						if (rsTrailer.getDate("last_updated") != null) {
							$mLast_Updated = rs.getDate("last_updated").toString();
							$mLast_Updated_By = rs.getString("last_updated_by");
							$mTo_Location_Code = rs.getString("to_location_code");
						} else {
							$mLast_Updated = null;
							$mLast_Updated_By = "";
							$mTo_Location_Code = "";
						}
					}
				}

				lineStr = rs.getString("customer_reference_code") + "," + rs.getDate("expected_delivery") + ","
						+ rs.getDate("date_created") + "," + $mParent_Tag + "," + $mLast_Updated + ","
						+ $mLast_Updated_By + "," + $mTo_Location_Code + "," + $mOrder_Type + "\r\n";


				wf.append(lineStr);
			}
			wf.close();
		} catch (SQLException sq) {
			sq.printStackTrace();

		} catch (IOException io) {
			io.printStackTrace();
		}
	}

}
