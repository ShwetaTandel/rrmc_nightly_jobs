package com.vantec.job.helper;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class celgard_weekly_report_helper {
	
		
	
	public static ResultSet celgard_getDetail(Connection conn,String partNumber) throws SQLException, IOException {
		
				//get dbconncetion
				//Connection conn = DBConnection.getConnection();
				//exceute first query
				String sql =     " select  t.date_created as date_created,part_number,haulier_reference,short_code,scanned_qty,t.customer_reference as customer_reference,ran_order,serial_reference from transaction_history t"
						 + " left join receipt_header on receipt_header.document_reference=t.document_reference"
			             + " where "
						 + " (short_code like 'PB%' OR short_code like 'RB%' OR short_code like 'SA%') and"
			             + " part_number = '"+partNumber+"'  order by date_created asc";
			                                                                                                                                                                                                                                                                                                                                                                                                      
				Statement st = conn.createStatement();
				ResultSet result  = st.executeQuery(sql);
				
				return result;
		
	}
	

	

	
	public static String getStatus(Connection conn,String partNumber,String serialNumber) throws SQLException, IOException {
		
		        //get dbconncetion
				//Connection conn = DBConnection.getConnection();
				//exceute first query
				String sql =     "  select status_code from inventory_master left join inventory_status on inventory_master.inventory_status_id=inventory_status.id"
						             + "  where part_number = '"+partNumber+"'  and serial_reference='"+serialNumber+"'";
				Statement st = conn.createStatement();
				ResultSet result  = st.executeQuery(sql);
				String status_code = null;
				while (result.next()) { 
					status_code=result.getString("status_code");
				}
				
				return status_code;
}
	

}
